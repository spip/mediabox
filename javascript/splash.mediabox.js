jQuery(function($){
	// poser le cookie au callback de la box chargee
	var set_cookie = function() {
		Cookies.set("modalsplash", spipConfig.mediabox.splash_url, { expires: spipConfig.mediabox.splash_days || 7 });
	}

	$.modalboxsplash = function(href, options) {
		if (spipConfig.mediabox.splash_iframe) {
			options = $.extend({
				type: 'iframe',
				width: spipConfig.mediabox.splash_width || '',
				height: spipConfig.mediabox.splash_height || ''
			}, options);
		}
		options = $.extend({className: 'splashbox'}, options);
		$.modalbox(href,$.extend({onShow:set_cookie},options));
	};
	// ouvrir la splash page si pas deja vue
	if (Cookies.get("modalsplash") != spipConfig.mediabox.splash_url) {
		$.modalboxsplash(spipConfig.mediabox.splash_url);
	}
});