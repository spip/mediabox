# Changelog
## Unreleased

### Changed

- Le menu de configuration de la modalbox se trouve désormais dans le menu « Apparence » et non plus « Squelette »

## 3.2.0 - 2025-11-26

### Added

- Installable en tant que package Composer

### Changed

- Compatible SPIP 5.0.0-dev
- #spip/spip6043
    ├── utiliser la méthode de chargement `_inits` et le nouveau formalisme de `ajaxCallback.js`
    └── migration de la configuration `window.mediabox_settings` depuis le `<head>` => `window.spipConfig.mediabox` vers `javascript/_inits/50_mediabox.js.html`

### Fixed

- #4883 Affichage de la modale médiabox sur petit écran

### Removed

- #spip/spip6043 `mediabox_echappe_js_config()`
